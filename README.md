# Pandora Website

Repository for the website <https://pandoragestiondocumental.es>.

## Technical details

- Static site made with [Gridsome](https://gridsome.org/) and [Buefy](https://buefy.org).
- Development dependencies can be found on `package.json`.
- Data is imported from the Markdown files in `/src/data` directory.
- Build is automatically run every time a new commit is pushed to branch `master`.

## Licenses

The code is released under [MIT License](https://mit-license.org/).
