---
title: Política de privacidad
id: 2
---

# Responsable del tratamiento de datos

El responsable del tratamiento de tus datos personales es:

**Pandora Gestión Documental, SL**  
NIF B05234059  
Paseo de la Castellana, 194
28046 Madrid

Puedes ponerte en contacto con el responsable del tratamiento dirigiendo un correo electrónico a la dirección <info@pandoragestiondocumental.es>.

# Datos recogidos y finalidad del tratamiento

Para navegar por nuestro sitio web no es necesario que facilites ningún dato personal.

Al contactar con nosotros a través de los formularios, el correo electrónico o el teléfono disponibles en la web, solicitamos datos personales, entre los cuales pueden figurar tu nombre y apellidos, tu dirección de correo electrónico, tu teléfono, el nombre de la empresa o institución en nombre de la cual escribes o tu CV. Utilizamos estos datos únicamente para responder tus consultas, solicitudes de soporte, reclamaciones o candidatura laboral, lo que puede incluir la elaboración y la presentación de una propuesta comercial personalizada en el caso de consultas relativas a nuestros servicios.

Estos datos no serán incorporados a una lista de distribución, pero sí podrán pasar a formar parte de nuestra base de datos de clientes si terminas formalizando un contrato con nosotros, con la finalidad de poder llevar a cabo el objetivo contractual entre las partes.

No recogemos, en ningún caso, datos que, por su naturaleza, sean particularmente sensibles en relación con los derechos y las libertades fundamentales, como tu filiación política o religiosa.

No cedemos estos datos a terceros ni los transferimos fuera de la UE, salvo obligaciones previstas en la legislación vigente (requerimientos judiciales, fiscales, entre otros).

Adicionalmente, al navegar por la web https://pandoragestiondocumental.es se pueden recoger datos no identificables, que pueden incluir, direcciones IP, ubicación geográfica (aproximada), un registro de cómo se utilizan los servicios y sitios, y otros datos que no pueden ser utilizados para identificar al usuario. Entre los datos no identificativos están también los relacionados a tus hábitos de navegación a través de servicios de terceros. Esta web utiliza los siguientes servicios de análisis de terceros:

- Google analytics: un servicio analítico de web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos (“Google”). Google Analytics utiliza “cookies”, que son archivos de texto ubicados en tu ordenador, para ayudarnos a analizar el uso que haces del sitio web. La información que genera la cookie acerca de tu uso de https://pandoragestiondocumental.es será directamente transmitida y archivada por Google en los servidores de Estados Unidos. La dirección IP es anonimizada, por lo que no será recopilada por Google.

Utilizamos esta información para analizar tendencias, administrar el sitio, rastrear los movimientos de los usuarios alrededor del sitio y para recopilar información demográfica sobre mi base de usuarios en su conjunto.

# Plazo de conservación

Los datos proporcionados se conservarán mientras se mantenga la relación comercial o durante los años necesarios para cumplir con las obligaciones legales.

# Medidas de seguridad

El tratamiento de los datos personales facilitados se lleva a cabo adoptando las medidas técnicas y organizativas necesarias para evitar la pérdida, uso indebido, alteración y acceso no autorizado a los mismos, habida cuenta del estado de la tecnología, la naturaleza de los datos y el análisis de riesgos efectuado.

Entre otras medidas, nuestro sitio web está protegido mediante un certificado SSL que cifra las comunicaciones entre tu navegador web y nuestros servidores. Puedes leer más sobre nuestro certificado en el sitio web de [Let’s Encrypt](https://letsencrypt.org/about/).

# Ejercicio de derechos

Puedes ejercer tus derechos de acceso, rectificación, oposición, supresión, limitación del tratamiento y portabilidad escribiéndonos a <info@pandoragestiondocumental.es>.

# Compromiso del usuario

Al contactar con nosotros, garantizas que eres mayor de 14 años y que la información facilitada es exacta y veraz.
