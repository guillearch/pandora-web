---
title: 4-Bibliotecas
---

Servicios bibliotecarios con **personal experto** en:

- Catalogación (Absysnet, Koha, etc.).
- Atención al público.
- Gestión de préstamos.
- Fondo antiguo.
- Depósitos.
- Actividades de difusión.
