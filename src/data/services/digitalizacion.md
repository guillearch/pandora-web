---
title: 3-Digitalización
---

La digitalización debe llevarse a cabo por 2 razones:

- **Conservar** aquellos documentos que sean vitales para la organización.
- **Agilizar** el flujo de trabajo

La tendencia se dirige a la presencia cada vez mayor de la llamada “**oficina sin papeles**”. Sin embargo, hasta que esto sea una realidad, el recurso utilizado es la digitalización de los documentos en papel, que debe garantizar la autenticidad del documento, preservar su calidad, así como su contenido.

Disponemos de **potentes equipos** para acometer proyectos de grandes volúmenes de documentos y **personal experto** en:

- **Captura** de imágenes de documentos en cualquier formato.
- **Migración** de soportes para evitar la pérdida de información por la obsolescencia
- **Indexación** de documentos digitalizados para su localización y recuperación mediante **OCR**.
- **Digitalización certificada** según normativa de la Agencia Tributaria para la validación de facturas.
