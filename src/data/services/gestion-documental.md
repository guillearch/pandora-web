---
title: 1-Gestión documental
---

No controlar adecuadamente la información o no conocer la mejor metodología para organizarla ni la **responsabilidad** que conlleva su acceso, son problemas con los que, a menudo, se encuentran las organizaciones. Un **tratamiento adecuado** de la documentación que se genera conlleva enormes **beneficios** tanto para las instituciones como para la sociedad en general. La gestión documental no debe ser un problema sino una ventaja.

Analizamos la situación documental de cada entidad para ofrecer soluciones a medida:

- **Análisis** legislativo y normativo (ISO 15489, 30300, etc).
- Definición de políticas e implementación de **modelos de gestión documental**.
- Implantación de **software** específico con experiencia destacada en soluciones como AtoM, Koha, DSpace, OpenKM, SharePoint Online, OpenProdoc, Alfresco, etc.
