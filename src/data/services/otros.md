---
title: 6-Otros
---

#### Grabación y tratamiento de datos

Damos cobertura profesional a empresas y organísmos públicos en relación a necesidades de **grabación de datos**, búsqueda y **recopilación** de información, elaboración de **estadísticas** y dossieres de **noticias**.

#### Inventarios de bienes y derechos

Especialmente enfocado a **Ayuntamientos**, aunque abierto a todo tipo de organismos, recopilamos la información acreditativa y descriptiva de cualquier tipo de bien o derecho.

Realizamos labores de **investigación** en Registros de la Propiedad, archivos históricos, Catastro, etc.

El resultado es la disposición de completos inventarios, clasificados por **epígrafes** en función del tipo de bien con información cartográfica y fotográfica de cada uno de ellos, tanto en formato **físico** como en **digital** a través de la instalación de software específico para su gestión.

#### Custodia

La **falta de espacio** por la acumulación de documentación es un problema constante en las empresas e instituciones. Un problema que, si no se solventa, tiende a ser mayor a medida que pasa el tiempo.

El espacio es escaso y caro y no disponer de él supone una **pérdida de dinero**.

Nos encargamos de la custodia de estos documentos en **instalaciones habilitadas** para este fin. Así las empresas ahorran, además de tener garantizada la **seguridad** de su documentación frente a robos, incendios, etc.

Por teléfono o vía correo electrónico podrán solicitar los documentos que necesiten y serán entregados en mano en un proceso perfectamente monitorizado.

#### Destrucción confidencial

Tras una **valoración y selección** de la documentación se decide que documentos van a **conservarse** de forma permanente en el archivo histórico y cuáles van a **eliminarse**.

Una vez realizado el **expurgo**, el paso siguiente es la **destrucción** que será realizada de forma confidencial.

El expurgo es importante a fin de evitar la destrucción de documentos con valor permanente.
