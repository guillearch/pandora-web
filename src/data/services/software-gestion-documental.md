---
title: 5-Software
---

Implantamos software de gestión documental, archivos y bibliotecas:

- AtoM.
- Koha.
- DSpace.
- OpenKM
- SharePoint Online.
- OpenProdoc.
